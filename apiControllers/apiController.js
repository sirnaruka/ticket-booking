const app = require('express').Router();
const queryObj = require('./queriesModel.js');

app.post('/getBookinghistory', function(req, res){
	var qry ="SELECT sheet_row,total_book,total_available FROM `booking_history` ";
	
	customQuery(qry,res);
});

app.post('/getbookedsheet', function(req, res){
	
	var qry ="SELECT GROUP_CONCAT(sheet_no) FROM `reservation_detail` Where status ='booked' ";
	
	customQuery(qry,res);
});
app.post('/reservation', function(req, res) {
	
	var query1 = "insert into `reservation_detail`(`booked_by`,`sheet_no`,`status`) values ";
	for (const [key, value] of Object.entries(req.body.reservation_data)) {
		query1 +="('"+value['name']+"','"+value['sheet_no']+"','"+value['status']+"'),";
	}
	query1 = query1.replace(/,\s*$/, "");
	var history_data = req.body.history_data;
	var query2 = "UPDATE `booking_history` SET `total_book`="+history_data['total_book']+",`total_available`="+history_data['total_available']+" WHERE `sheet_row` = '"+history_data['sheet_row']+"' ";
	//console.log(query2);console.log(query1);
	reqParam = {
		table_query:[query1,query2],
		
	}
	
	multiquery(reqParam,res);
});


function customQuery(reqParam, res){
	
	queryObj.custom(reqParam,function(rs){
		//console.log(rs); return false;
		if(rs.status==0){
			res.send({status: 0,'message': 'Please Provide correct parameter.'});
		}else{
			res.send({status: 1,'message': 'parameter found.','result':rs.result});
		}
		
	});
}

function multiquery(reqParam, res){
	
	queryObj.multi(reqParam,function(rs){
		
		if(rs.status==0){
			res.send({status: 0,'message': 'Please Provide correct parameter.'});
		}else{
			res.send({status: 1,'message': 'parameter found.','result':rs.result});
		}
		
	});
}

module.exports = app;
