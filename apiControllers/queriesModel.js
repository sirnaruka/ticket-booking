
var mysql = require('mysql'); 
var db = mysql.createPool({  
    host: 'localhost',  
    user: 'root',  
    password: '',  
    database: 'booking_system'  
}); 

module.exports = {
   custom:function(qry,ret){
		db.query(qry, function(err, result) {
            if (err) throw err;
            if (result != '') {
				ret({
                    status: 1,
                    'message': 'Success',
                    result:result
                });
            } else {
                ret({
                    status: 0,
                    'message': 'No Record Found !'
                });
            }
        });
	},
	multi:function(arr,ret){
		//console.log(arr);
		var res = [];
		var pending = arr.table_query.length;
		
		arr.table_query.forEach(function(key,val){
			var qry = key;
			
			db.query(qry, function(err, result) {
				if (err) throw err;
				if (result != '') {
					res.push(result);
					 if( 0 === --pending ) {
						 //console.log(res);console.log(123);
						ret({
							status: 1,
							'message': 'Success',
							result:res
						}); //callback if all queries are processed
					}
				} else {
					ret({
						status: 0,
						'message': 'No Record Found !'
					});
				}
			});
			
		});
		
	}
}
