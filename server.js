const express = require('express'),
  path = require('path'),
  bodyParser = require('body-parser'),
  cors = require('cors');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
const port = process.env.PORT || 4000;

const api = require('./apiControllers/apiController');
//  Connect all our api to our application
app.use('/', api);
const server = app.listen(port, function () {
  console.log('Start Now with localhost:' + port);
});
