import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { BackendapiService } from '../../services/backendapi.service';
@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {
booked_data : any; sheet_data : any; 
  constructor(private http: HttpClient,private router: Router,private apiservices: BackendapiService, private fb: FormBuilder) { }

  ngOnInit() {
	  let ob;
	  ob = {'data':'all'};
    this.apiservices.postData('getbookedsheet', ob).subscribe((data) => {
          this.displaydata(data,'bookedsheet');
        });
		let sheetarray;
		sheetarray = {
			'r1':[1,2,3,4,5,6,7],
			'r2':[8,9,10,11,12,12,13,14],
			'r3':[15,16,17,18,19,20,21],
			'r4':[22,23,24,25,26,27],
			'r5':[28,29,30,31,32,33,34,35],
			'r6':[36,37,38,39,40,41,42],
			'r7':[43,44,45,46,47,48,49],
			'r8':[50,51,52,53,54,55,56],
			'r9':[57,58,59,60,61,62,63],
			'r10':[64,65,66,67,68,69,70],
			'r11':[71,72,73,74,75,76,77],
			'r12':[78,79,80],
			};
    this.sheet_data = sheetarray;
	console.log(this.sheet_data);
  }
  clickEvent(){
    this.router.navigate(['/']);
}
  displaydata(data,type) {
    if (data.result !== '') {
		if(type == 'bookedsheet'){
			var str = data.result[0]['GROUP_CONCAT(sheet_no)'];
			var ar = str.split(',');
			//var ar =Array.ConvertAll<string, int>(str.Split(','), Convert.ToInt32)
			var result = ar.map(function (x) { 
			  return parseInt(x, 10); 
			});
			this.booked_data =  result;
			console.log(this.booked_data);
		}
      
    } else {
      
       this.router.navigate(['/booking']);
      
      // location.reload();
    }
  }

}
