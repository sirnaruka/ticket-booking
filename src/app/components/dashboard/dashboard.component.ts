import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { BackendapiService } from '../../services/backendapi.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
usersdata;booking_history : any; 
 reservationForm: FormGroup;
 
  constructor(private http: HttpClient,private router: Router,private apiservices: BackendapiService, private fb: FormBuilder) {
		this.createForm();

  }
  createForm() {
    this.reservationForm = this.fb.group({
      name: ['', Validators.required ],
      sheet_no: ['', Validators.required ]
   });
   
  }

  ngOnInit() {
	  let ob;
	  ob = {'data':'all'};
    this.apiservices.postData('getBookinghistory', ob).subscribe((data) => {
          this.displaydata(data,'Bookinghistory');
        });
    
  }
  
  
  reservationsubmit(formvalue) {
	  let string = '';
    if (this.reservationForm.valid) {
		this.alotsheet(formvalue);
      
    } else {
      this.validateAllFormFields(this.reservationForm);
    }
  }
  
  alotsheet(formvalue) {
	  
	  var name = formvalue.name;
	  var sheet_no = formvalue.sheet_no;
	  var total_avail =0;var total_booked =0;var sheet_row_value ='';
	  for (const [key, value] of Object.entries(this.booking_history)) {
		  
		  if(value['total_available'] >= sheet_no){
			   total_avail = value['total_available'];
			   total_booked = value['total_book'];
			   sheet_row_value = value['sheet_row'];
			   var couch_no = key;
			  break;
		  }
		  
		}
		
		var couch_info =[1,8,15,22,29,36,43,50,57,64,71,78];
		var reservation_data = [];
		let history_data;
		if(sheet_row_value !=''){
			
			var sheet_start_alot_no = couch_info[couch_no] + total_booked;
			var sheet_end_alot_no = sheet_start_alot_no + sheet_no;
			var i;
			console.log(sheet_start_alot_no);console.log(sheet_end_alot_no);console.log(couch_no);
			for (i = sheet_start_alot_no; i < sheet_end_alot_no; i++) {
			  reservation_data.push({"name": name, "sheet_no": i,"status":"booked"});
			}
			
			var remaining = total_avail - sheet_no;
			var booked = total_booked + sheet_no;
			history_data = {'sheet_row':sheet_row_value,"total_available":remaining,"total_book":booked};
			let obj;
		
			obj = {'reservation_data':reservation_data,"history_data":history_data};
			console.log(this.booking_history);console.log(obj);
			
		
		  
			this.apiservices.postData('reservation',obj).
			subscribe(
			  (data) => { 
			  this.displaydata(data,"reservation");
			  console.log(data);
			  }
			);
		}else{
			alert("No sheet available");
		}
		
  }
displaydata(data,type) {
    if (data.result !== '') {
		if(type == 'Bookinghistory'){
			this.booking_history = data.result;
			
		}else{
			console.log('yes');
			 this.router.navigate(['/booking']);
		}
      
    } else {
      //this.toasterService.pop('error', 'Error!', 'Your username or sheet_no was incorrect.');
       this.router.navigate(['/']);
      
      // location.reload();
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
