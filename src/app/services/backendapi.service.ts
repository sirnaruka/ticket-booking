import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class BackendapiService {

  constructor(private http: HttpClient,private route: ActivatedRoute,
    private router: Router) { }
  uri = 'http://localhost:4000/';

  postData(action,reqParm){
    return this.http.post(this.uri+action, reqParm);
  }
  pageStatus(){
	return this.route.url;
  }
}