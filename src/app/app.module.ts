import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DisplayComponent } from './components/display/display.component';

import { BackendapiService } from './services/backendapi.service';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DisplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	HttpClientModule, ReactiveFormsModule, FormsModule
  ],
  providers: [BackendapiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
